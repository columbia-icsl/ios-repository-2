//
//  RecommenderViewController.swift
//  EnergyApp
//
//  Created by Peter Wei on 1/22/18.
//  Copyright © 2018 Rong Zhou. All rights reserved.
//

import UIKit
import UserNotifications
class RecommenderViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UNUserNotificationCenterDelegate {
    var refresher:UIRefreshControl!
    
    //@IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var collectionLayout: UICollectionView!
    
    
    let array:[String] = ["1", "2", "3", "4"]
    let headerArray:[String] = ["Move to 1003A", "Move to Space 10M", "Hello", "Shift Schedule +30 min"]
    let footerArray:[String] = ["Savings: 1201 Wh", "Savings: 520 Wh", "SOMETHING", "Savings: 660 Wh"]
    var recommendations:[String] = []
    var body:[String] = []
    var reward:[Int] = []
    var message:[String] = []
    var type:[Int] = []
    let colorArray:[UIColor] = [UIColor(displayP3Red: 72.0/255, green: 201.0/255, blue: 176.0/255, alpha: 1.0), UIColor(displayP3Red: 235.0/255, green: 152.0/255, blue: 78.0/255, alpha: 1.0), UIColor(displayP3Red: 244.0/255, green: 208.0/255, blue: 63.0/255, alpha: 1.0), UIColor(displayP3Red: 205.0/255, green: 97.0/255, blue: 85.0/255, alpha: 1.0)]
    var testNumber = 0
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recommendations.count
    }
    
    @objc func pressed(sender: UIButton!) {
        let section = sender.tag / 100
        let row = sender.tag % 100
        let indexPath = NSIndexPath(row: row, section: section)//NSIndexPath(row: row, inSection: section)
        let suggestionAlert = UIAlertController(title: "Accept Suggestion?", message: self.body[(indexPath as NSIndexPath).row], preferredStyle: UIAlertControllerStyle.alert)
        suggestionAlert.addAction(UIAlertAction(title: "Decline", style: .default, handler: {(action: UIAlertAction!) in
            let UUID = UIDevice.current.identifierForVendor!.uuidString
            var data = UUID as String
            data += ","
            data += self.message[indexPath.row]
            data += ","
            data += String(self.reward[indexPath.row])
            data += ","
            data += "False"
            self.message.remove(at:(indexPath as NSIndexPath).row)
            self.reward.remove(at:(indexPath as NSIndexPath).row)
            self.recommendations.remove(at:(indexPath as NSIndexPath).row)
            self.type.remove(at:(indexPath as NSIndexPath).row)
            self.body.remove(at:(indexPath as NSIndexPath).row)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {self.collectionLayout!.reloadData()})
            let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/suggestionDecisions/")
            let session = URLSession.shared //create a url connection
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            
            let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
            let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
            {(data,response,error) in
                //WARNING: Might need a dispatchAsync here, haven't tested
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print(error?.localizedDescription ?? "sending decision error")
                    return
                }
                if (data == nil) {
                    //print("no data returned!")
                    return
                }
                _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
            });
            task.resume()
        }))
        suggestionAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {(action: UIAlertAction!) in}))
        suggestionAlert.addAction(UIAlertAction(title: "Accept", style: .default, handler: {(action: UIAlertAction!) in
            let UUID = UIDevice.current.identifierForVendor!.uuidString
            var data = UUID as String
            data += ","
            data += self.message[indexPath.row]
            data += ","
            data += String(self.reward[indexPath.row])
            data += ","
            data += "True"
            self.message.remove(at:(indexPath as NSIndexPath).row)
            self.reward.remove(at:(indexPath as NSIndexPath).row)
            self.recommendations.remove(at:(indexPath as NSIndexPath).row)
            self.type.remove(at:(indexPath as NSIndexPath).row)
            self.body.remove(at:(indexPath as NSIndexPath).row)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {self.collectionLayout!.reloadData()})
            let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/suggestionDecisions/")
            let session = URLSession.shared //create a url connection
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            
            let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
            let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
            {(data,response,error) in
                //WARNING: Might need a dispatchAsync here, haven't tested
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print(error?.localizedDescription ?? "sending decision error")
                    return
                }
                if (data == nil) {
                    //print("no data returned!")
                    return
                }
                _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
            });
            task.resume()
        }))
        
    
        self.present(suggestionAlert, animated: true, completion: nil)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RCell", for: indexPath) as! CollectionViewCell
        if self.type[indexPath.row] == 0 {
            cell.recImage.image = UIImage(named:"man1")
            cell.recImageBack.image = UIImage(named:"man1")
        }
        else if self.type[indexPath.row] == 1 {
            cell.recImage.image = UIImage(named:"Clock")
            cell.recImageBack.image = UIImage(named:"Clock")
        }
        else if self.type[indexPath.row] == 2 {
            cell.recImage.image = UIImage(named:"Power")
            cell.recImageBack.image = UIImage(named:"Power")
        }
        else if self.type[indexPath.row] == 3 {
            cell.recImage.image = UIImage(named:"Shade")
            cell.recImageBack.image = UIImage(named:"Shade")
        }
        else {
            cell.recImage.image = UIImage(named:"clock1")
            cell.recImageBack.image = UIImage(named:"clock1")
        }
        cell.AcceptButton.setTitle("Accept Recommendation", for: .normal)
        cell.AcceptButton.tag = (indexPath.section*100)+indexPath.row
        cell.AcceptButton.addTarget(self, action: #selector(pressed(sender:)), for: .touchUpInside)
        cell.cardF.backgroundColor = colorArray[indexPath.row]
        cell.cardB.backgroundColor = colorArray[indexPath.row]
        cell.recImage.layer.cornerRadius = 2
        cell.recImage.layer.masksToBounds = false
        //cell.headerLabel.text = headerArray[indexPath.row]
        cell.footerLabel.text = "Reward: " +  String(self.reward[indexPath.row])
        cell.headerLabel.text = self.recommendations[indexPath.row]
        cell.cardF.layer.cornerRadius = 2
        cell.cardF.layer.masksToBounds = false
        cell.cardF.layer.shadowPath = UIBezierPath(rect:cell.cardF.bounds).cgPath
        cell.cardF.layer.shadowColor = UIColor.black.cgColor
        cell.cardF.layer.shadowOpacity = 0.12
        cell.cardF.layer.shadowOffset = CGSize(width:0.0, height:2.0)
        cell.cardF.layer.shadowRadius = 2.0
        cell.cardB.layer.cornerRadius = 2
        cell.cardB.layer.masksToBounds = false
        cell.cardB.layer.shadowPath = UIBezierPath(rect:cell.cardB.bounds).cgPath
        cell.cardB.layer.shadowColor = UIColor.black.cgColor
        cell.cardB.layer.shadowOpacity = 0.12
        cell.cardB.layer.shadowOffset = CGSize(width:0.0, height:2.0)
        cell.cardB.layer.shadowRadius = 2.0
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refresher = UIRefreshControl()
        self.collectionLayout!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.attributedTitle = NSAttributedString(string: "Fetching recommendations...")
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionLayout!.refreshControl = self.refresher
        self.collectionLayout!.refreshControl?.endRefreshing()
        let itemSize = UIScreen.main.bounds.width/2-9
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(20, 3, 10, 3)
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 3
        collectionLayout.collectionViewLayout = layout
        // Do any additional setup after loading the view.
        
        let _ = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(RecommenderViewController.loadRecommendations), userInfo: nil, repeats: true)
        
    }

    @objc func loadData() {
        loadRecommendations()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {self.collectionLayout!.refreshControl?.endRefreshing()})
    }
    
    
    @objc func loadRecommendations() {
        let userUUID = UIDevice.current.identifierForVendor!.uuidString
        let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/getRecs/?id=\(userUUID)")
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, err in
            if data == nil {
                return
            }
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
            DispatchQueue.main.async {
                if (dataString == nil) {
                    return
                }
                self.recommendations.removeAll()
                self.body.removeAll()
                self.type.removeAll()
                self.reward.removeAll()
                self.message.removeAll()
                do {
                    
                    print(dataString)
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                    let balance = json["balance"] as? Int
                    if (balance != nil) {
                        print(balance ?? -1)
                        // -----> TODO: self.balanceLabel.text = String(balance!)
                        let tempBalance = json["tempBalance"] as? Int
                        if (tempBalance != nil) {
                            let rem = (balance! % 20)
                            userStatus.userBalance = rem
                            let left = min(Double(tempBalance!),Double(20-rem))
                            userStatus.tempRewards = left
                        }
                    }
                    let location = json["location"] as? String
                    // ------> TODO: self.addLocationButton.setTitle(location, for: UIControlState())
                    //self.locationLabel.text = location
                    //print(location)
                    if let suggestions = json["suggestions"] as? [[String:AnyObject]] {
                        for suggestion in suggestions {
                            if let type = suggestion["type"] as? String {
                                switch type {
                                case "move":
                                    self.parseJson(suggestion: suggestion, suggestionType: 0)
                                    break
                                case "shift":
                                    self.parseJson(suggestion: suggestion, suggestionType:1)
                                    break
                                case "plug":
                                    self.parseJson(suggestion: suggestion, suggestionType:2)
                                    break
                                case "shade":
                                    self.parseJson(suggestion: suggestion, suggestionType:3)
                                    break
                                default:
                                    self.parseJson(suggestion: suggestion, suggestionType: 4)
                                    break
                                }
                            }
                        }
                    }
                    
                    self.collectionLayout.reloadData()
                } catch {
                    print("Error Serializing JSON: \(error)")
                }
            }
        })
        task.resume()
    }
    
    @objc func parseJson(suggestion:[String:AnyObject], suggestionType:Int) {
        if let title = suggestion["title"] as? String {
            self.recommendations.append(title)
        }
        if let b = suggestion["body"] as? String {
            self.body.append(b)
        }
        
        self.type.append(suggestionType)
        
        if let reward = suggestion["reward"] as? Int {
            self.reward.append(reward)
        }
        if let messageID = suggestion["messageID"] as? String {
            self.message.append((messageID as NSString) as String)
        }
        print(self.recommendations)
        print(self.message)
        print(self.type)
        if let n = suggestion["notification"] as? Int {
            print("suggestion")
            print(n)
            if (n != 0) {
                let center = UNUserNotificationCenter.current()
                center.delegate = self
                center.requestAuthorization(options: [.alert, .sound], completionHandler: {(granted, error) in })
                let content = UNMutableNotificationContent()
                content.title = "ICSL Energy has received 1 energy saving recommendation."
//                content.body = b
                content.categoryIdentifier = "alarm"
                content.sound = UNNotificationSound.default()
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
                let request = UNNotificationRequest(identifier: "Hello", content: content, trigger: trigger)
                center.add(request)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
