//
//  RewardViewController.swift
//  EnergyApp
//
//  Created by Rong Zhou on 1/20/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit
import Charts

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ChartViewDelegate,UIPopoverPresentationControllerDelegate{
    @objc let appGrey = UIColor(red:0x79, green:0x7B, blue:0x84)
    @objc let appGreen = UIColor(red:0x65, green:0xD2, blue:0xAC)
    @objc let appRed = UIColor(red:0xD6, green:0x68, blue:0x53)
    @objc let appWhite = UIColor(red:0xFC, green:0xFA, blue:0xFA)
    @objc let appBlueGrey = UIColor(red:0x21, green:0x96, blue:0xF3)
    @objc let appBlack = UIColor(red:0x21, green:0x21, blue:0x21)
    
    @IBOutlet var rewardCard: UIView!
    
    @IBOutlet var rewardHeader: UIView!
    
    
    @IBOutlet var suggestionsCard: UIView!
    
    @IBOutlet var suggestionsHeader: UIView!
    
    
    @IBOutlet var giftCard: UIView!
    
    @IBOutlet var giftHeader: UIView!
    
    @objc func dropShadow(card: UIView) {
        card.layer.cornerRadius = 2
        card.layer.masksToBounds = false
        card.layer.shadowPath = UIBezierPath(rect:card.bounds).cgPath
        print(card.bounds)
        card.layer.shadowColor = UIColor.black.cgColor
        card.layer.shadowOpacity = 0.12
        card.layer.shadowOffset = CGSize(width:0.0, height:2.0)
        card.layer.shadowRadius = 2.0
        
    }
    
    @objc func roundHeaderCorners(header: UIView) {
        let rectShape = CAShapeLayer()
        rectShape.bounds = header.frame
        rectShape.position = header.center
        rectShape.path = UIBezierPath(roundedRect:header.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii:CGSize(width:2, height:2)).cgPath
        header.layer.mask = rectShape
    }
    
    @IBOutlet var background: UIImageView!
//    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet var rewardFrac: UILabel!
    @objc let stripeColor = UIColor(patternImage: UIImage(named:"Stripe")!)
    @objc let rewardLimit = 20
    @objc let rewardColor = UIColor(red:0x29, green:0x73, blue:0x73)
    @IBOutlet var rewardRing: PieChartView!
    @IBOutlet var tableView: UITableView!
    @objc let suggestionCellIdentifier = "suggestionCell"
    @objc var showcase:iShowcase!
    
    @IBOutlet var amazon: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()


        //initializeCircle()
//        tabBar.selectedItem = tabBar.items?[0]
        self.amazon.layer.cornerRadius = 5.0
        self.amazon.layer.masksToBounds = false
        self.amazon.clipsToBounds = true
        self.rewardChartSetup()
        self.tableView.reloadData()

        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        dropShadow(card: self.rewardCard)
        dropShadow(card: self.giftCard)
        dropShadow(card: self.suggestionsCard)
        
        roundHeaderCorners(header: self.rewardHeader)
        roundHeaderCorners(header: self.giftHeader)
        roundHeaderCorners(header: self.suggestionsHeader)
    }
    
    //Setup the reward chart
    @objc func rewardChartSetup() {

        self.rewardRing.delegate = self
        self.rewardRing.noDataText = "Loading Data..."
        self.rewardRing.noDataFont = UIFont(name:"Futura (Light)", size: UIScreen.main.bounds.width/15)
        self.rewardRing.noDataTextColor = appBlack
        self.rewardRing.backgroundColor = UIColor.clear
        self.rewardRing.holeColor = UIColor.clear
        self.rewardRing.holeRadiusPercent = 0.9
        
        self.rewardRing.legend.enabled = false
        self.rewardRing.chartDescription?.enabled = false
        self.rewardRing.centerText = "\(userStatus.userBalance)/\(self.rewardLimit)"
        self.rewardRing.highlightPerTapEnabled = false
        self.makePieChart()
        let myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15), NSAttributedStringKey.foregroundColor: appBlack]
        self.rewardRing.centerAttributedText = NSAttributedString(string: "\(userStatus.userBalance)/\(self.rewardLimit)"
            , attributes: myAttributes )
    }

////////////////////Popover//////////////////
    @IBAction func popover(_ sender: Any) {
        self.performSegue(withIdentifier: "showView", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showView")
        {
            let vc = segue.destination
            let controller = vc.popoverPresentationController
            if (controller != nil){
                controller?.delegate = self
                self.view.alpha = 0.4;
            }
        }
    }
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        self.view.alpha = 1.0;
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
//////////////////////////

    //Called every time the rewards are updated

    @objc func makePieChart() {
        var dataEntries: [ChartDataEntry] = [ChartDataEntry]()
        dataEntries.append(ChartDataEntry(x: 0, y: Double(userStatus.userBalance)))
        dataEntries.append(ChartDataEntry(x: 1, y: Double(userStatus.tempRewards)))
        dataEntries.append(ChartDataEntry(x: 1, y: max(0.0, 20.0-Double(userStatus.tempRewards) - Double(userStatus.userBalance))))
        let rewardChartDataSet = PieChartDataSet(values: dataEntries, label: "rewardChart")
        rewardChartDataSet.drawValuesEnabled = false
        var dataSets: [PieChartDataSet] = [PieChartDataSet]()
        dataSets.append(rewardChartDataSet)
        let pieChartData = PieChartData(dataSets: dataSets)
        self.rewardRing.data = pieChartData
        let colors: [UIColor] = [appBlueGrey, stripeColor, appGrey]
        rewardChartDataSet.colors = colors
        self.rewardRing.centerText = "\(userStatus.userBalance)/\(self.rewardLimit)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.rewardChartSetup()
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Table View for holding the suggestions
    //TODO: Another table view for the feed
    //TODO: Link tab view to table to switch between feed and suggestions
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let suggestionAlert = UIAlertController(title: "Accept Suggestion?", message: userStatus.suggestionDescription[(indexPath as NSIndexPath).row], preferredStyle: UIAlertControllerStyle.alert)
        let reward = String(userStatus.rewardNumber[(indexPath as NSIndexPath).row])
        let messageID = String(userStatus.messageIDs[(indexPath as NSIndexPath).row])
        suggestionAlert.addAction(UIAlertAction(title: "Decline", style: .default, handler: {(action: UIAlertAction!) in}))
        
        //If the User accepts the suggestion, remove all userStatus data associated with that suggestion, and send to the server api that we have accepted a suggestion
        suggestionAlert.addAction(UIAlertAction(title: "Accept", style: .default, handler: {(action: UIAlertAction!) in
            userStatus.tempRewards += Double(userStatus.rewardNumber[(indexPath as NSIndexPath).row])
            userStatus.suggestedActions.remove(at: (indexPath as NSIndexPath).row)
            userStatus.suggestionDescription.remove(at: (indexPath as NSIndexPath).row)
            userStatus.rewardNumber.remove(at: (indexPath as NSIndexPath).row)
            self.tableView.reloadData()
            self.makePieChart()
            let myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15), NSAttributedStringKey.foregroundColor: UIColor.white]
            self.rewardRing.centerAttributedText = NSAttributedString(string: "\(userStatus.userBalance)/\(self.rewardLimit)"
            , attributes: myAttributes)
            
            let UUID = UIDevice.current.identifierForVendor!.uuidString
            var data = UUID as String
            data += ","
            data += messageID
            data += ","
            data += reward
            let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/suggestionDecisions/")
            let session = URLSession.shared //create a url connection
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            
            let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
            let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
                {(data,response,error) in
                    //WARNING: Might need a dispatchAsync here, haven't tested
                    guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                        print(error?.localizedDescription ?? "sending decision error")
                        return
                    }
                    if (data == nil) {
                        //print("no data returned!")
                        return
                    }
                    _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
            });
            task.resume()
            }))
        
        self.present(suggestionAlert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userStatus.suggestedActions.count
//        return 5
    }
    
    var imageNames = [UIImage(named: "Motion"), UIImage(named:"Sync"), UIImage(named:"Power-1"), UIImage(named:"Change"), UIImage(named:"miscEnergy")]
    //TODO: DISPLAY MORE INFORMATION ABOUT THE SUGGESTIONS
    //Currently: displays the suggestion titles only
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: suggestionCellIdentifier, for: indexPath) as! SuggestionsTableViewCell
//        let myAttributes = [NSFontAttributeName: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15)]
        cell.suggestionDescription.text = userStatus.suggestedActions[(indexPath as NSIndexPath).row]
        cell.suggestionDescription.font = UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/20)
        cell.suggestionImage.image = self.imageNames[(indexPath as NSIndexPath).row]
        cell.suggestionReward.text = String(userStatus.rewardNumber[(indexPath as NSIndexPath).row])
        return cell
    }
}



















