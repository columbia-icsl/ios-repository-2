//
//  ProfileTableViewCell.swift
//  EnergyApp
//
//  Created by Peter Wei on 3/22/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet var ButtonLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
