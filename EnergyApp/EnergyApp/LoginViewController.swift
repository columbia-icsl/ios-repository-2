//
//  LoginViewController.swift
//  EnergyApp
//
//  Created by Peter Wei on 3/9/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit
class LoginViewController: UIViewController {

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!

    @IBOutlet var backdrop: UIView!
    
    @IBOutlet var login: UIButton!
    
    
    @IBAction func loginCheck(_ sender: Any) {
        if (emailTextField.text! == "") {
            let refreshAlert = UIAlertController(title: "Alert", message: "Please enter your email!", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                
            }))
            present(refreshAlert, animated: true, completion: nil)
            return
        }
        if (passwordTextField.text! == "") {
            let refreshAlert = UIAlertController(title: "Alert", message: "Please enter your password!", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                
            }))
            present(refreshAlert, animated: true, completion: nil)
            return
        }
        let UUID = UIDevice.current.identifierForVendor!.uuidString
        var data = UUID as String
        data += ","
        data += emailTextField.text!
        data += ","
        //data += passwordTextField.text!
        ///////////////////////////////////////////
        let hash = SHA1.hexString(from: passwordTextField.text!)
        data += hash!
        ///////////////////////////////////////////

        let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/userManagement/login/")
        let session = URLSession.shared //create a url connection
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
        let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
            {(data,response,error) in
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print(error?.localizedDescription ?? "URL Response Login Failed")
                    return
                }
                DispatchQueue.main.async {
                    if (data == nil) {
                        print("no data returned!")
                        return
                    }
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
                    if (dataString == "0") {
                        self.performSegue(withIdentifier: "loginToTab", sender: self)
                    }
                    if (dataString == "1") {
                        let refreshAlert = UIAlertController(title: "Alert", message: "Email or Password Incorrect!", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                            
                        }))
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                    if (dataString == "404") {
                        let refreshAlert = UIAlertController(title: "Alert", message: "Device Not Found.", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                            
                        }))
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
        });
        
        task.resume()

    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.layer.cornerRadius = 7
        passwordTextField.layer.cornerRadius = 7

        login.layer.cornerRadius = login.layer.frame.height/2

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
