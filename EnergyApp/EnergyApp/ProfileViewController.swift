//
//  ProfileViewController.swift
//  EnergyApp
//
//  Created by Peter Wei on 3/9/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var profileImage: UIImageView!

    @IBOutlet var NameLabel: UILabel!
    
  
    @IBOutlet var labNameLabel: UILabel!

    @IBOutlet var energySavedLabel: UILabel!
    
    
    @IBOutlet var suggestionsTakenLabel: UILabel!
    
    
    @IBOutlet var rewardsTakenLabel: UILabel!
    
    @IBOutlet var aboutUsButton: UIButton!
    
    @IBOutlet var viewTutorialButton: UIButton!

    @IBOutlet var buttonAppearance: UIButton!
    @IBOutlet var background: UIImageView!
    @IBAction func logoutButton(_ sender: Any) {
        

    }
    @objc let profileCellIdentifier = "ProfileCell"
    @objc let buttonLabels: [String] = ["About Us", "Tutorial", "Log Out"]
    override func viewDidLoad() {
        super.viewDidLoad()
        NameLabel.text = userStatus.userName
        energySavedLabel.text = "\(userStatus.savedEnergy) W"
        self.profileImage.layer.cornerRadius = self.profileImage.layer.frame.width / 2
        self.profileImage.layer.borderWidth = 5.0
        self.profileImage.layer.borderColor = UIColor.white.cgColor
        self.profileImage.clipsToBounds = true

        
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as! ProfileTableViewCell
        //        let myAttributes = [NSFontAttributeName: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15)]
        cell.ButtonLabel.text = buttonLabels[(indexPath as NSIndexPath).row]
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            self.performSegue(withIdentifier: "showAboutUs", sender: self)
        } else if (indexPath.row == 1) {
            self.performSegue(withIdentifier: "profileToTutorial", sender: self)
        } else if (indexPath.row == 2) {
            let UUID = UIDevice.current.identifierForVendor!.uuidString
            let data = UUID as String
            let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/userManagement/logout/")
            let session = URLSession.shared //create a url connection
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
            let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
                {(data,response,error) in
                    guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                        print(error?.localizedDescription ?? "URL Response Login Failed")
                        return
                    }
                    DispatchQueue.main.async {
                        if (data == nil) {
                            print("no data returned!")
                            return
                        }
                        let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
                        if (dataString == "0") {
                            self.performSegue(withIdentifier: "loggedOut", sender: self)
                        } else {
                            let refreshAlert = UIAlertController(title: "Alert", message: "Logout Failed, try again", preferredStyle: UIAlertControllerStyle.alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                                
                            }))
                            self.present(refreshAlert, animated: true, completion: nil)
                        }
                    }
            });
            
            task.resume()
        } else {
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
