//
//  RegisterViewController.swift
//  EnergyApp
//
//  Created by Peter Wei on 2/2/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit

class TextField : UITextField {
    override var tintColor: UIColor! {
        didSet {
            setNeedsDisplay()
        }
    }
    override func draw(_ rect: CGRect) {
        
        let startingPoint   = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint     = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 2.0
        
        tintColor.setStroke()
        
        path.stroke()
    }
}

extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
}
extension UIView {
    @objc func addBackground(_ str: String) {
        // screen width and height:
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageViewBackground.image = UIImage(named: str)
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
        //imageViewBackground.alpha = 0.5

        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style:.dark))
        visualEffectView.frame = imageViewBackground.bounds
        imageViewBackground.addSubview(visualEffectView)
        let visualEffectView1 = UIVisualEffectView(effect: UIVibrancyEffect())
        visualEffectView1.frame = imageViewBackground.bounds
        imageViewBackground.addSubview(visualEffectView1)
        self.addSubview(imageViewBackground)
        self.sendSubview(toBack: imageViewBackground)
    }}

class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var backdrop: UIView!
    @IBOutlet var register: UIButton!
    @IBOutlet var backgroundPhoto: UIImageView!
    @IBOutlet var password: UITextField!
    //REGISTRATION PAGE: SEND THE USERNAME TO THE SERVER
    @IBOutlet var userfullnameField: UITextField!
    @IBOutlet var usernameField: UITextField!
    
    @IBAction func shouldReturn(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    
    @IBAction func RegisterButton(_ sender: UIButton) {
        
        
        if (usernameField.text! == "") {
            let refreshAlert = UIAlertController(title: "Alert", message: "Please enter your name!", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                
            }))
            present(refreshAlert, animated: true, completion: nil)
            return
        }
        if (userfullnameField.text! == "") {
            let refreshAlert = UIAlertController(title: "Alert", message: "Please enter your email!", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                
            }))
            present(refreshAlert, animated: true, completion: nil)
            return
        }
        if (password.text! == "") {
            let refreshAlert = UIAlertController(title: "Alert", message: "Please enter a password!", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                
            }))
            present(refreshAlert, animated: true, completion: nil)
            return
        }
        let UUID = UIDevice.current.identifierForVendor!.uuidString
        var data = UUID as String
        data += ","
        data += usernameField.text!
        data += ","
        data += userfullnameField.text!
        data += ","
        
        //data += password.text!
        ///////////////////////////////////////////
        let hash = SHA1.hexString(from: password.text!)
        data += hash!
        ///////////////////////////////////////////
        
        let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/userManagement/newUser/")
        let session = URLSession.shared //create a url connection
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
        let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
            {(data,response,error) in
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print(error?.localizedDescription ?? "URL Response Login Failed")
                    return
                }
                DispatchQueue.main.async {
                    if (data == nil) {
                        print("no data returned!")
                        return
                    }
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
                    if (dataString == "0") {
                        self.performSegue(withIdentifier: "toTutorial", sender: self)
                    }
                    if (dataString == "Username Taken") {
                        let refreshAlert = UIAlertController(title: "Alert", message: "Username is already taken. Choose another username!", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(action: UIAlertAction!) in
                            
                        }))
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                }
        });
        
        task.resume()
    }

    @IBOutlet var passwordTextField: TextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        register.layer.cornerRadius = register.layer.frame.height/2
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
