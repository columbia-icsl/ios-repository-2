//
//  DashViewController.swift
//  EnergyApp
//
//  Created by Rong Zhou on 1/20/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreFoundation
import UserNotifications
import Charts

class myValueFormatter: NSObject, IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(value) W"
    }
}

class timeValueFormatter: NSObject, IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let mod = 0
        let date = NSDate()
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.hour, .minute, .second], from: date as Date)
        var hour = components.hour
        var minute = components.minute
        let cur = mod + ((userStatus.HVACvals.count-1)/6) * 6
        //print((cur - Int(value)) % 6)
        if ((mod - Int(value)) % 6 != 0) {
            return ""
        } else {
            let num = (cur-Int(value))/6
            if (num > minute!) {
                hour = hour! - 1
            }
            minute = (minute!-num + 60) % 60
            var minString = ""
            if (minute! < 10) {
                minString = "0\(minute!)"
            } else {
                minString = "\(minute!)"
            }
            return "\(hour!):" + minString
        }
    }
}
//Persistent Data structure for the whole application
struct userStatus {
    static var userBalance = 0
    static var userName:String = ""
    static var savedEnergy:Int = 0
    static var suggestedActions: Array<String> = Array<String>() //List containing suggestion titles
    static var suggestionTypes: Array<Int> = Array<Int>() //List of suggestion types
    static var suggestionDescription: Array<String> = Array<String>() //List containing suggestion descriptions
    static var rewardNumber: Array<Int> = Array<Int>() //List containing the number of rewards for each suggestion
    static var vals: Array<Double> = Array<Double>() //Historical list of user's energy footprint
    static var electricVals: Array<Double> = Array<Double>() //Historical list of user's electrical footprint
    static var lightVals: Array<Double> = Array<Double>() //Light footprint
    static var HVACvals: Array<Double> = Array<Double>() //HVAC footprint
    static var electric: Double = 0.0 //current electric value (equal to electricVals[electricVals.count-1]
    static var HVAC: Double = 0.0 //current HVAC value
    static var light: Double = 0.0 //current light value
    static var energyValue: Double = 0.0 //current energy value
    static var tempRewards: Double = 0.0 //temporary reward count, before validation
    static var messageIDs: Array<String> = Array<String>() //unique suggestion ID's to prevent duplicates
}

class MarkerView: UIView {


    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
}

class FirstViewController: UIViewController, CBCentralManagerDelegate, UNUserNotificationCenterDelegate, ChartViewDelegate,UIPopoverPresentationControllerDelegate {
    
    
    @objc let markerView=MarkerView()
    
    @IBOutlet var rewardCard: UIView!
    
    @IBOutlet var footprintCard: UIView!
    
    @IBOutlet var EnergyCard: UIView!
    
    @IBOutlet var rewardView: UIView!

    @IBOutlet var rewardHeader: UIView!
    
    @IBOutlet var footprintHeader: UIView!
    
    @IBOutlet var energyHeader: UIView!
    
    @IBOutlet var EnergyCardBack: UIView!
    
    @IBOutlet var energyBackHeader: UIView!
    
    @IBOutlet var energyBackTitle: UILabel!
    
    @IBOutlet var powerView: UIView!
    @IBOutlet var background: UIImageView!

    @IBOutlet var energyCardContainer: UIView!
    
    @IBOutlet var HVACPercent: UILabel!
    
    @IBOutlet var LightPercent: UILabel!
    
    @IBOutlet var ElectricPercent: UILabel!
    
    @IBOutlet var HVACLabel: UILabel!
    
    @IBOutlet var LightLabel: UILabel!
    
    @IBOutlet var ElectricLabel: UILabel!
    
    
    
    @IBAction func footprintChangeTime(_ sender: Any) {
        let toView = UIView()
        toView.frame = footprintCard.frame
        toView.backgroundColor = UIColor.red
        UIView.transition(from: footprintCard!, to: toView, duration: 1, options: .transitionCurlUp, completion: nil)
        footprintCard?.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @objc var cardFront: UIView? = nil
    @objc var cardBack:UIView? = nil
    //VARIABLE DECLARATIONS
    @objc let appGrey = UIColor(red:0x79, green:0x7B, blue:0x84)
    @objc let appGreen = UIColor(red:0x65, green:0xD2, blue:0xAC)
    @objc let appRed = UIColor(red:0xD6, green:0x68, blue:0x53)
    @objc let appWhite = UIColor(red:0xFC, green:0xFA, blue:0xFA)
    @objc let appBlueGrey = UIColor(red:0x21, green:0x96, blue:0xF3)
    @objc let appBlack = UIColor(red:0x21, green:0x21, blue:0x21)
    
    @objc let color1 = UIColor(red:0xD6, green:0x68, blue:0x53) //deep red
    @objc let color2 = UIColor(red:0x63, green:0x78, blue:0x7C)
    @objc let color3 = UIColor(red:0x53, green:0x13, blue:0x1E)
    @objc let rewardColor = UIColor(red:0x29, green:0x73, blue:0x73)
    @objc let stripeColor = UIColor(patternImage: UIImage(named:"Stripe")!) //stripe color
    @objc var centralManager: CBCentralManager! //Central Manager
    @objc var measurementTime:CUnsignedLongLong = 0
    @objc var measurementTracking: Array<CUnsignedLongLong> = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                         0]
    @objc var RSSIvalues: Array<NSNumber> = [-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,
                                       -100,-100,-100,-100,-100,-100,-100,-100,-100,-100,
                                       -100,-100,-100,-100,-100,-100,-100,-100,-100,-100,
                                       -100,-100,-100,-100,-100,-100,-100,-100,-100,-100,
                                       -100] //IMPORTANT! List of RSSI values
    @objc var suggestionDecision: Array<Int> = Array<Int>()
    @objc var highlighted: Array<Bool> = Array<Bool>()
    @objc var suggestionTimes: Array<Date> = Array<Date>()
    
    @IBOutlet var energyDisplay: UILabel!
    @IBOutlet var lineChartView: LineChartView!
    @objc var ringBufferLimit = 40
    var ringBuffer:[(beaconNum:Int, RSSIval:Int)] = []
    @objc var ringBufferPointer = 0 //pointer to next place to store value
    @objc var sendTime = Date() //last time we sent info to server
    @objc var setup = false
    @objc let rewardLimit = 20
    @objc let energyLimit = 3000
    @objc let times = [60, 55]
    @objc let vals = [1, 2, 4, 6, 3, 5, 4, 6, 7, 8, 10, 15]
    @IBOutlet var rewardNum: UILabel!
    @IBOutlet weak var EnergyValue: UILabel!
    @IBOutlet weak var EnergyCircle: UIView!
    
    @IBOutlet var rewardPieChart: PieChartView!
    @IBOutlet var energyPieChart: PieChartView!
    ///////Popover///////
    

    @IBAction func popover(_ sender: Any) {
        self.performSegue(withIdentifier: "showView", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showView")
        {
            let vc = segue.destination
            let controller = vc.popoverPresentationController
            if (controller != nil){
                controller?.delegate = self
                self.view.alpha = 0.4;
            }
        }
    }
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        self.view.alpha = 1.0;
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    /////////////////////
    //BEGIN CLASS FUNCTIONS
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print(entry)
        let mPos = lineChartView.getMarkerPosition(highlight: highlight)
        print(mPos)
        let markerPos = chartView.getMarkerPosition(highlight: highlight)
        print(markerPos)
        //self.markerView.backgroundColor = UIColor.blue
        //self.markerView.center = CGPoint(x:CGFloat(highlight.x), y:markerView.center.y)
        //self.markerView.isHidden = false
    }
    @objc func dropShadow(card: UIView) {
        card.layer.cornerRadius = 2
        card.layer.masksToBounds = false
        card.layer.shadowPath = UIBezierPath(rect:card.bounds).cgPath
        print(card.bounds)
        card.layer.shadowColor = UIColor.black.cgColor
        card.layer.shadowOpacity = 0.12
        card.layer.shadowOffset = CGSize(width:0.0, height:2.0)
        card.layer.shadowRadius = 2.0
    }
    
    @IBAction func flipBack(_ sender: Any) {
        let toView = cardFront
        UIView.transition(from: cardBack!, to: toView!, duration: 1, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        cardBack?.translatesAutoresizingMaskIntoConstraints = false
    }

    @IBAction func flip(_ sender: Any) {
        let toView = cardBack
        UIView.transition(from: cardFront!, to: toView!, duration: 1, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
        cardFront?.translatesAutoresizingMaskIntoConstraints = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.rewardPieChartSetup()
        self.energyPieChartSetup()
        self.lineChartSetup()
        
        
        
        
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: [CBCentralManagerOptionRestoreIdentifierKey: "myCentralManager", CBCentralManagerOptionShowPowerAlertKey: true]) //initialize central manager, set state restoration name (STATE RESTORATION STEP)
        /*let screenBounds = UIScreen.main.bounds
        print(screenBounds.width/2)
        print(rewardView.frame.width)
        self.rewardView.frame.size.width = screenBounds.width/2
        let newFrame:CGRect = CGRect(x: rewardView.frame.origin.x, y: rewardView.frame.origin.y, width: screenBounds.width/2, height: rewardView.frame.height)
        self.rewardView.frame = newFrame*/
        //REPEATED CALLS:
        //callEnergyAPI pulls energy data from the server every 4 seconds, and updates the line graph
        let _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(FirstViewController.callEnergyAPI), userInfo: nil, repeats: true)
        
        //makePieChart refreshes the two pie charts
        let _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(FirstViewController.makePieChart), userInfo: nil, repeats: true)
    }
    
    @objc func roundHeaderCorners(header: UIView) {
        let rectShape = CAShapeLayer()
        rectShape.bounds = header.frame
        rectShape.position = header.center
        rectShape.path = UIBezierPath(roundedRect:header.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii:CGSize(width:2, height:2)).cgPath
        header.layer.mask = rectShape
    }
    override func viewDidAppear(_ animated: Bool) {
        dropShadow(card: self.rewardCard)
        dropShadow(card: self.EnergyCard)
        dropShadow(card: self.footprintCard)
        dropShadow(card: self.EnergyCardBack)
        
        roundHeaderCorners(header: self.rewardHeader)
        roundHeaderCorners(header: self.footprintHeader)
        roundHeaderCorners(header: self.energyHeader)
        roundHeaderCorners(header: self.energyBackHeader)
        
    }
    
    ////////////////////////////
    //SETUP CHARTS FUNCTIONS
    @objc func rewardPieChartSetup() {
        self.rewardPieChart.delegate = self
        self.rewardPieChart.noDataText = "Loading Data..."
        self.rewardPieChart.noDataFont = UIFont(name:"Futura (Light)", size: UIScreen.main.bounds.width/15)
        self.rewardPieChart.noDataTextColor = self.appBlack
        self.rewardPieChart.backgroundColor = UIColor.clear
        self.rewardPieChart.holeColor = UIColor.clear
        self.rewardPieChart.holeRadiusPercent = 0.9
        self.rewardPieChart.legend.enabled = false
        self.rewardPieChart.chartDescription?.enabled = false
        self.rewardPieChart.highlightPerTapEnabled = false
       //self.rewardPieChart.centerText = "\(userStatus.userBalance)/\(self.rewardLimit)"
        let myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/20), NSAttributedStringKey.foregroundColor: self.appBlack]
        self.rewardPieChart.centerAttributedText = NSAttributedString(string: "\(userStatus.userBalance)/\(self.rewardLimit)"
            , attributes: myAttributes)
    }
    
    @objc func energyPieChartSetup() {
        self.cardFront = self.EnergyCard
        self.cardBack = self.EnergyCardBack
        self.HVACPercent.textColor = color1
        self.LightPercent.textColor = color2
        self.ElectricPercent.textColor = color3
        self.HVACLabel.textColor = color1
        self.LightLabel.textColor = color2
        self.ElectricLabel.textColor = color3
        self.energyPieChart.delegate = self
        self.energyPieChart.noDataText = "Loading Data..."
        self.energyPieChart.noDataFont = UIFont(name:"Futura (Light)", size: UIScreen.main.bounds.width/15)
        self.energyPieChart.noDataTextColor = self.appBlack
        self.energyPieChart.backgroundColor = UIColor.clear
        self.energyPieChart.holeColor = UIColor.clear
        self.energyPieChart.legend.enabled = false
        self.energyPieChart.chartDescription?.enabled = false
        self.energyPieChart.holeRadiusPercent = 0.9
        self.energyPieChart.highlightPerTapEnabled = false
        let myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/20), NSAttributedStringKey.foregroundColor: self.appBlack]
        self.energyPieChart.centerAttributedText = NSAttributedString(string: "\(userStatus.energyValue) W"
            , attributes: myAttributes)
        self.makePieChart()

    }
    
    @objc func lineChartSetup() {
        self.lineChartView.delegate = self
        self.lineChartView.noDataText = "Loading Data..."
        self.lineChartView.noDataFont = UIFont(name:"Futura (Light)", size: UIScreen.main.bounds.width/15)
        self.lineChartView.noDataTextColor = appBlack
        self.lineChartView.chartDescription?.text = "Tap to view"
        self.lineChartView.chartDescription?.textColor = appBlack
        self.lineChartView.gridBackgroundColor = UIColor.clear
        self.lineChartView.xAxis.labelTextColor = appBlack
        self.lineChartView.leftAxis.labelTextColor = appBlack
        self.lineChartView.rightAxis.labelTextColor = appBlack
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.backgroundColor = UIColor.clear
        self.lineChartView.doubleTapToZoomEnabled = false
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.rightAxis.drawGridLinesEnabled = false
        //self.lineChartView.leftAxis.drawGridLinesEnabled = false
        self.lineChartView.rightAxis.drawAxisLineEnabled = false
        self.lineChartView.leftAxis.drawAxisLineEnabled = false
        self.lineChartView.drawBordersEnabled = false
        self.lineChartView.xAxis.drawAxisLineEnabled = false
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.rightAxis.drawLabelsEnabled = false
        self.lineChartView.extraRightOffset = 10.0
        self.lineChartView.dragEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.leftAxis.axisMinimum = 0.0
        self.lineChartView.leftAxis.valueFormatter = myValueFormatter()
        self.lineChartView.xAxis.valueFormatter = timeValueFormatter()
        self.setup = false
        self.lineChartView.drawMarkers = true
        //let marker:BalloonMarker = BalloonMarker(color: UIColor.red, font: UIFont(name:"Helvetica", size:12)!, textColor: UIColor.green, insets: UIEdgeInsets(top:7.0, left:7.0, bottom:7.0, right:7.0))
        //marker.minimumSize = CGSize(width: 75.0, height: 35.0)
        
        //self.lineChartView.marker = marker
        self.lineChartView.xAxis.setLabelCount(13, force: true)

    }
    
    @objc func makePieChart() {
        if (userStatus.energyValue == 0) {
            self.HVACPercent.text = "0 W"
            self.LightPercent.text = "0 W"
            self.ElectricPercent.text = "0 W"
        } else {
            let HVACper = userStatus.HVAC
            self.HVACPercent.text = "\(HVACper) W"
            let Lightper = userStatus.light
            self.LightPercent.text = "\(Lightper) W"
            let Electricper = userStatus.electric
            self.ElectricPercent.text = "\(Electricper) W"
        }

        //SET DATA FOR REWARD PIE CHART, called every 4 seconds
        var myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15), NSAttributedStringKey.foregroundColor: appBlack]
        self.rewardPieChart.centerAttributedText = NSAttributedString(string: "\(userStatus.userBalance)/\(self.rewardLimit)", attributes: myAttributes)

        var dataEntries: [ChartDataEntry] = [ChartDataEntry]()
        dataEntries.append(ChartDataEntry(x: 0, y: Double(userStatus.userBalance)))
        dataEntries.append(ChartDataEntry(x: 1, y: Double(userStatus.tempRewards)))
        dataEntries.append(ChartDataEntry(x: 2, y: max(0.0, 20.0-Double(userStatus.tempRewards) - Double(userStatus.userBalance))))
        let rewardChartDataSet = PieChartDataSet(values: dataEntries, label: "rewardChart")
        rewardChartDataSet.drawValuesEnabled = false
        var dataSets: [PieChartDataSet] = [PieChartDataSet]()
        dataSets.append(rewardChartDataSet)
        let pieChartData = PieChartData(dataSets: dataSets)
        self.rewardPieChart.data = pieChartData
        let colors: [UIColor] = [appBlueGrey, self.stripeColor, appGrey]
        rewardChartDataSet.colors = colors
        
        //SET DATA FOR ENERGY PIE CHART
        myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15), NSAttributedStringKey.foregroundColor: appBlack]
        self.energyPieChart.centerAttributedText = NSAttributedString(string: "\(userStatus.energyValue) W"
            , attributes: myAttributes)
        var energyEntries: [ChartDataEntry] = [ChartDataEntry]()
        energyEntries.append(ChartDataEntry(x: 0, y: Double(userStatus.energyValue)))
        energyEntries.append(ChartDataEntry(x: 1, y: Double(max(0, 3000.0-userStatus.energyValue))))
        let energyChartDataSet = PieChartDataSet(values: energyEntries, label: "energyChart")
        energyChartDataSet.drawValuesEnabled = false
        var energyDataSets: [PieChartDataSet] = [PieChartDataSet]()
        energyDataSets.append(energyChartDataSet)
        let energyChartData = PieChartData(dataSets: energyDataSets)
        self.energyPieChart.data = energyChartData
        let energyColors:[UIColor] = [appRed, appGrey]
        energyChartDataSet.colors = energyColors
    }
    
    //THIS IS ONLY CALLED ONCE! This is to set up the chart data. Unlike the pie charts, which can be reset every time new data is pulled, line chart should only be set up once and data appended
    @objc func setChartData(times: [Int]) {
        print("first time")
        var HVACVals: [ChartDataEntry] = [ChartDataEntry]()
        var lightVals: [ChartDataEntry] = [ChartDataEntry]()
        var electricVals: [ChartDataEntry] = [ChartDataEntry]()
        for i in 0...(userStatus.HVACvals.count-1) {
            electricVals.append(ChartDataEntry(x: Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i] + userStatus.electricVals[i])))
            lightVals.append(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i])))
            HVACVals.append(ChartDataEntry(x: Double(i), y: Double(userStatus.HVACvals[i])))
        }
        let set1: LineChartDataSet = LineChartDataSet(values: HVACVals, label: "HVAC Set")
        let set2: LineChartDataSet = LineChartDataSet(values: lightVals, label: "Light Set")
        let set3: LineChartDataSet = LineChartDataSet(values: electricVals, label: "Electric Set")
        set1.drawValuesEnabled = false
        set2.drawValuesEnabled = false
        set3.drawValuesEnabled = false
        
        
        set1.fill = Fill(color: color1)
        set1.drawFilledEnabled = true
        set2.fill = Fill(color: color2)
        set2.drawFilledEnabled = true
        set3.fill = Fill(color: color3)
        set3.drawFilledEnabled = true
        
        
        set1.setColor(color1)
        set1.setCircleColor(appGrey)
        set1.lineWidth = 2.0
        set1.circleRadius = 3.0
        set1.fillAlpha = 150/255.0
        set1.highlightColor = appGrey
        set1.drawCircleHoleEnabled = true
        set2.setColor(color2)
        set2.setCircleColor(appGrey)
        set2.lineWidth = 2.0
        set2.circleRadius = 3.0
        set2.fillAlpha = 150/255.0
        set2.highlightColor = appGrey
        set2.drawCircleHoleEnabled = true
        set3.setColor(color3)
        set3.setCircleColor(appGrey)
        set3.lineWidth = 2.0
        set3.circleRadius = 3.0
        set3.fillAlpha = 150/255.0
        set3.highlightColor = appGrey
        set3.drawCircleHoleEnabled = true
        var dataSets: [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(set1)
        dataSets.append(set2)
        dataSets.append(set3)
        let data:LineChartData = LineChartData(dataSets: dataSets)
        data.setValueTextColor(appGrey)
        self.lineChartView.data = data
        
        self.lineChartView.data?.notifyDataChanged()
        self.lineChartView.moveViewToX(0)
        self.lineChartView.setVisibleXRange(minXRange: Double(12.0), maxXRange: Double(12.0))
    }
    
    //APPENDS DATA TO LINE CHART, called every time after setChartData
    @objc func updateCounter() {
        let i = userStatus.HVACvals.count-1
        print(i)
        self.lineChartView.data?.addEntry(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i])), dataSetIndex: 2)
        self.lineChartView.data?.addEntry(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i])), dataSetIndex: 1)
        self.lineChartView.data?.addEntry(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i] + userStatus.electricVals[i])), dataSetIndex: 0)
        //let _ = self.lineChartView.data?.removeEntry(xValue: Double(i-13), dataSetIndex: 0)
        //let _ = self.lineChartView.data?.removeEntry(xValue: Double(i-13), dataSetIndex: 1)
        //let _ = self.lineChartView.data?.removeEntry(xValue: Double(i-13), dataSetIndex: 2)
        self.lineChartView.data?.notifyDataChanged()
        self.lineChartView.notifyDataSetChanged()
        let start = i
        self.lineChartView.moveViewToAnimated(xValue: Double(start), yValue: Double(userStatus.HVACvals[i]), axis: YAxis.AxisDependency.left, duration: 0.8, easingOption: ChartEasingOption.easeInSine)
        self.lineChartView.setVisibleXRange(minXRange: Double(12.0), maxXRange: Double(12.0))
    }
    
    //Here we call the energy api, and get a json of the total footprint, electric portion, HVAC portion, and lighting portion
    @objc func callEnergyAPI() {
        let userUUID = UIDevice.current.identifierForVendor!.uuidString
        let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/appSupport/?id=\(userUUID)")
        let session = URLSession.shared //create a url connection
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, err in
            if data == nil {
                return
            }
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
            DispatchQueue.main.async {
                if (dataString == nil) {
                    return
                }
                do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                let value = json["value"] as? Float
                let HVAC = json["HVAC"] as? Double
                let light = json["Light"] as? Double
                let electric = json["Electrical"] as? Double
                    if (value == nil || HVAC == nil || light == nil || electric == nil) {
                        return
                    } else {
                        userStatus.electric = electric!
                        userStatus.energyValue = round(Double(value!))
                        userStatus.HVAC = HVAC!
                        userStatus.light = light!
                        if (self.setup == false) { //
                            self.setup = true
                            for _ in 0...12 {
                                userStatus.electricVals.append(userStatus.electric)
                                userStatus.HVACvals.append(userStatus.HVAC)
                                userStatus.vals.append(userStatus.energyValue)
                                userStatus.lightVals.append(userStatus.light)
                            }
                            self.setChartData(times:self.times)
                            //self.initializeCircle()
                        } else {
                            userStatus.electricVals.append(userStatus.electric)
                            userStatus.HVACvals.append(userStatus.HVAC)
                            userStatus.vals.append(userStatus.energyValue)
                            userStatus.lightVals.append(userStatus.light)
                            self.updateCounter()
                        }
                    }
                } catch {
                    //print("THE REAL ERROR!")
                    print("error serializing JSON: \(error)")
                }
            }
            })
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    //Called every time the device receives a message from a bluetooth beacon
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        //let BLEserviceUUID = [CBUUID(string: "BEC26202-A8D8-4A94-80FC-9AC1DE37DAA6")] //service UUID (represents Tx power, one of the services advertised by the sBeacons)
        let BLEserviceUUID = [CBUUID(string: "1804"), CBUUID(string: "BEC26202-A8D8-4A94-80FC-9AC1DE37DAA6")]
        //transmit power, manufacturer data
        if (central.state == .poweredOn)
        {
            self.centralManager?.scanForPeripherals(withServices: BLEserviceUUID, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true]) //duplicates are allowed
        }
        else
        {
            // do something like alert the user that ble is not on
        }
    }
    //if the beacon signal is received
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if (peripheral.name == nil) {
            return
        }
        var ID = peripheral.identifier.uuidString //get the UUID of the beacon

        let manufacturerData = advertisementData["kCBAdvDataManufacturerData"] //additional ID data!?!?
        if (manufacturerData == nil) {
            return
        }
    
        let MData = manufacturerData as! Data
        var dataBytes = [UInt8](repeating: 0, count: MData.count)
        (MData as NSData).getBytes(&dataBytes, length:dataBytes.count)
        let hexString = NSMutableString() //get the "new" ID data
        var byteCount = 0
        for byte in dataBytes { //only take the first x bytes of the manufacturer data, guaranteed to be constant for each beacon
            byteCount += 1
            hexString.appendFormat("%02x", UInt(byte))
            if (byteCount >= 8) {
                break
            }
        }
        ID = hexString as String
        
        //Ring buffer, holds up to 100 (?) beacon values
        for (index, p) in UUIDs.enumerated() {
            if p == ID {
                if (RSSI as! Int > 0) {
                    break
                }
                if (ringBuffer.count < ringBufferLimit) {
                    ringBuffer.append((index, Int(RSSI)))
                    ringBufferPointer += 1
                } else {
                    ringBuffer[ringBufferPointer] = (index, Int(RSSI))
                    ringBufferPointer += 1
                }
                ringBufferPointer = ringBufferPointer % ringBufferLimit
                break
            }
        }
        let elapsedTime = Date()
        let duration = Int(elapsedTime.timeIntervalSince(self.sendTime))
        if (duration < 10) {
            return //don't send to server
        } else {
            self.sendTime = elapsedTime //only send if more than 10 seconds have passed, in order to offload stress on the server
        }
        
        var averageRSSI = Array(repeating: 0, count: 41)
        var numAverages = Array(repeating: 0, count: 41) //number of samples found for each count
        for sample in ringBuffer {
            let beaconNum = sample.0
            let RSSIval = sample.1
            averageRSSI[beaconNum] += RSSIval
            numAverages[beaconNum] += 1
        }
        for (i, _) in averageRSSI.enumerated() {
            if (numAverages[i] == 0) {
                averageRSSI[i] = -100 //no samples
            } else {
                averageRSSI[i] = averageRSSI[i]/numAverages[i] //take the average
            }
        }
        
        //URL to POST to
        let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/Beacons/")
        let session = URLSession.shared //create a url connection
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let UUID = UIDevice.current.identifierForVendor!.uuidString
        var data = UUID as String

        for sigStr in averageRSSI {
            data += ","
            data += String(sigStr)//sigStr.stringValue //add bluetooth beacon RSSI value to the string
        }
        let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
        let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
            {(data,response,error) in
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print(error?.localizedDescription ?? "Localization URL Response Failed")
                    //print("here is the error.")
                    return
                }
                DispatchQueue.main.async {
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //here is the returned information
                    if (dataString == nil) {
                        return
                    }
                    userStatus.suggestionTypes.removeAll()
                    userStatus.suggestedActions.removeAll()
                    self.suggestionDecision.removeAll()
                    // -------> TODO: self.cells.removeAll()
                    userStatus.rewardNumber.removeAll()
                    self.suggestionTimes.removeAll()
                    userStatus.suggestionDescription.removeAll()
                    userStatus.messageIDs.removeAll()
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                        let balance = json["balance"] as? Int
                        if (balance != nil) {
                            //print(balance ?? -1)
                            // -----> TODO: self.balanceLabel.text = String(balance!)
                            let tempBalance = json["tempBalance"] as? Int
                            if (tempBalance != nil) {
                                    let rem = (balance! % 20)
                                    userStatus.userBalance = rem
                                    let left = min(Double(tempBalance!),Double(20-rem))
                                    userStatus.tempRewards = left
                            }
                        }
                        //let location = json["location"] as? String
                        // ------> TODO: self.addLocationButton.setTitle(location, for: UIControlState())
                        //print(location)
                        if let suggestions = json["suggestions"] as? [[String:AnyObject]] {
                            for suggestion in suggestions {
                                if let type = suggestion["type"] as? String {
                                    switch type {
                                    case "move":
                                        self.parseJson(suggestion: suggestion, suggestionType: 0)
                                        break
                                    case "change":
                                        self.parseJson(suggestion: suggestion, suggestionType: 1)
                                        break
                                    case "turnoff":
                                        self.parseJson(suggestion: suggestion, suggestionType: 2)
                                        break
                                    case "sync":
                                        self.parseJson(suggestion: suggestion, suggestionType: 3)
                                        break
                                    default:
                                        self.parseJson(suggestion: suggestion, suggestionType: 4)
                                        break
                                    }
                                }
                            }
                        }
                    } catch {
                        print("error serializing JSON: \(error)")
                    }
                }
            }
        );
        task.resume()
    }
    @objc func parseJson(suggestion:[String:AnyObject], suggestionType:Int) {
        userStatus.suggestionTypes.append(suggestionType)
        if let title = suggestion["title"] as? String {
            userStatus.suggestedActions.append(title)
        }
        self.suggestionDecision.append(1)
        self.suggestionTimes.append(Date())
        let body = suggestion["body"] as? String
        if (body != nil) {
            userStatus.suggestionDescription.append(body!)
        }
        if let reward = suggestion["reward"] as? Int {
            userStatus.rewardNumber.append(reward)
        }
        if let messageID = suggestion["messageID"] as? String {
            userStatus.messageIDs.append((messageID as NSString) as String)
        }
        if let n = suggestion["notification"] as? Int {
            if (n != 0) {
                let center = UNUserNotificationCenter.current()
                center.delegate = self
                center.requestAuthorization(options: [.alert, .sound], completionHandler: {(granted, error) in })
                let content = UNMutableNotificationContent()
                content.title = "Building Energy"
                content.body = body!
                content.categoryIdentifier = "alarm"
                content.sound = UNNotificationSound.default()
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                let request = UNNotificationRequest(identifier: "Hello", content: content, trigger: trigger)
                center.add(request)
            }
        }
    }
    @objc let UUIDs = ["f900016e04e64984", "f90001690d1fac13", "f90001799c2ce28b", "f9000169a6d0e462", "f900015acb890ba4", "f90001925ceed04f", "f90001445622a8b5", "f900018a16945342", "f90001535b96b23f", "f90001ced6688472",
                 "f9000151ca814933", "f9000154265338ec", "f900018ff0d461ce", "f900018ff0d461ce", "f90001b5e1f2a607",
                 "f90001cdf639767e", "f90001c99a18ea37", "f90001bd1866d42a", "f90001dd870b0d1e", "f900010218cb2e63",
                 "f900012720b39dde", "f900017e9d0fb84e", "f90001fbee27a1cf", "f90001cbea3a1904", "f9000143e013e15e",
                 "f900018d9a41d879", "f90001665d0025a8", "f9000177c9214b4a", "f900015c274b097b", "f90001ec526f628f",
                 "f90001b6d0ec2da2", "f900017b053062d4", "f90001e11c126311", "f90001435cbdac75", "f90001173b78520c",
                 "f900015bc6df9635", "f90001be8b0e254e", "f90001e541ec397e", "f9000151a9bc0719", "f900012edca22762",
                 "f900013b3f9eb6ef"]
}

