//
//  CheckRegistrationViewController.swift
//  EnergyApp
//  Class for checking the registration at app start up. If the user is already registered, go straight to the dashboard
//  Created by Peter Wei on 2/4/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit


class CheckRegistrationViewController: UIViewController {
    @objc var signInTimer: Timer? = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        let oldPassword = "Crf#2016china"
        let hash = SHA1.hexString(from: oldPassword)
        print("New Password is: \n")
        print(hash)
        checkRegistration()
        signInTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CheckRegistrationViewController.checkRegistration), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    
    //We first check the user management api for if the user has already registered, if so then go to the dashboard (just have to send the device ID)
    @objc func checkRegistration() {
        let UUID = UIDevice.current.identifierForVendor!.uuidString
        let data = UUID as String
        let url = URL(string: "http://icsl.ee.columbia.edu:8000/api/userManagement/returnUser/")
        let session = URLSession.shared //create a url connection
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let data1 = (data as NSString).data(using: String.Encoding.utf8.rawValue) //encode the data
        let task = session.uploadTask(with: request as URLRequest, from: data1, completionHandler:
            {(data,response,error) in
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print(error?.localizedDescription ?? "URL Response Error!")
                    return
                }
                DispatchQueue.main.async {
                    if (data == nil) {
                        print("no data returned!")
                        return
                    }
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]

                        let name = json["username"] as? String
                        let status = json["status"] as? String
                        let energySaved = json["energySaved"] as? Int
                        userStatus.userName = name!
                        userStatus.savedEnergy = energySaved!
                        if (status == "0") {
                            
                            //IF SUCCESS: GO TO THE DASHBOARD, segue is "checkSuccess"
                            self.performSegue(withIdentifier: "loggedIn", sender: self)
                        } else if (status == "1") {
                            self.performSegue(withIdentifier: "toLogin", sender: self)
                        } else if (status == "404") {
                            //IF FAILED: GO TO THE LOGIN PAGE, segue is "checkFailed"
                            self.performSegue(withIdentifier: "toRegistration", sender: self)
                        }
                        self.signInTimer?.invalidate()
                        self.signInTimer = nil
                    } catch {
                        //print("THE REAL ERROR!")
                        print("error serializing JSON: \(error)")
                    }
                }
        });
        
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
