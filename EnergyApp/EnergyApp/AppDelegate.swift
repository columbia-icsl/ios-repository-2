//
//  AppDelegate.swift
//  EnergyApp
//
//  Created by Rong Zhou on 1/20/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//


        

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font:UIFont(name: "Futura (Light)", size: 13)!], for: .normal)
        let pageControl: UIPageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor(red: 0, green: 124, blue: 149)
        pageControl.currentPageIndicatorTintColor = UIColor(red:200, green:200, blue:200)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

}

