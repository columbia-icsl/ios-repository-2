//
//  CollectionViewCell.swift
//  EnergyApp
//
//  Created by Peter Wei on 1/22/18.
//  Copyright © 2018 Rong Zhou. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @objc var cardFront: UIView? = nil
    @objc var cardBack:UIView? = nil
    
    @IBOutlet var cardB: UIView!
    @IBOutlet var cardF: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.cardFront = self.cardF
        self.cardBack = self.cardB
    }
    
    @IBAction func flipBack(_ sender: Any) {
        print("flip back")
        let toView = self.cardFront
        UIView.transition(from: self.cardBack!, to: toView!, duration: 1, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        self.cardBack?.translatesAutoresizingMaskIntoConstraints = false
    }
    @IBAction func flip(_ sender: Any) {
        print("flip")
        let toView = self.cardBack
        UIView.transition(from: self.cardFront!, to: toView!, duration: 1, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
        self.cardFront?.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @IBOutlet weak var recImage: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var footerLabel: UILabel!
    @IBOutlet weak var recImageBack: UIImageView!
    @IBOutlet weak var AcceptButton: UIButton!
    
}
