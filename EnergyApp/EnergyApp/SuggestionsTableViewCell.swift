//
//  SuggestionsTableViewCell.swift
//  EnergyApp
//
//  Created by Peter Wei on 2/4/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit

class SuggestionsTableViewCell: UITableViewCell {
    @IBOutlet var suggestionDescription: UILabel!
    
    @IBOutlet var suggestionImage: UIImageView!
    
    @IBOutlet var suggestionReward: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
