//
//  EnergyViewController.swift
//  EnergyApp
//
//  Created by Rong Zhou on 1/20/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

//EXTENSION TO MAKE UIColors IN HEX EASIER
extension UIColor {
    @objc convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    @objc convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}



import UIKit
import Charts

class EnergyViewController: UIViewController, ChartViewDelegate,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet var background: UIImageView!
    //VARIABLE DECLARATIONS
    @objc let appGrey = UIColor(red:0x79, green:0x7B, blue:0x84)
    @objc let appGreen = UIColor(red:0x65, green:0xD2, blue:0xAC)
    @objc let appRed = UIColor(red:0xD6, green:0x68, blue:0x53)
    @objc let appWhite = UIColor(red:0xFC, green:0xFA, blue:0xFA)
    @objc let appBlueGrey = UIColor(red:0x21, green:0x96, blue:0xF3)
    @objc let appBlack = UIColor(red:0x21, green:0x21, blue:0x21)
    @objc let color1 = UIColor(red:0xD6, green:0x68, blue:0x53)
    @objc let color2 = UIColor(red:0x63, green:0x78, blue:0x7C)
    @objc let color3 = UIColor(red:0x53, green:0x13, blue:0x1E)
    //let color1 = UIColor(red:0xE7, green:0x7E, blue:0x99)
    //let color2 = UIColor(red:0xF8, green:0xF0, blue:0xC8)
    //let color3 = UIColor(red:0xA0, green:0xD5, blue:0xFF)
    @objc var reset = false
    @objc var piereset = false
    @IBOutlet var ElectricPercent: UILabel!
    @IBOutlet var LightPercent: UILabel!
    @IBOutlet var HVACpercent: UILabel!
    @IBOutlet var lineChartView: LineChartView!

    @IBOutlet var electricLabel: UILabel!
    @IBOutlet var lightLabel: UILabel!
    @IBOutlet var HVAClabel: UILabel!
    @IBOutlet var pieChartView: PieChartView!
    @IBOutlet weak var Circle: UIView!
    
    
    
    //CLASS FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.energyPieChartSetup()
        self.energyLineChartSetup()
        
        //update the line chart every 4 seconds, in line with the chart on the dashboard
        let _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(EnergyViewController.makeChart), userInfo: nil, repeats: true)
        
        //update the pie chart every 4 seconds
        let _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(EnergyViewController.makePieChart), userInfo: nil, repeats: true)
    }
    
    //FUNCTIONS FOR SETTING UP CHARTS, ONLY CALLED ONCE
    @objc func energyPieChartSetup() {
        self.HVACpercent.textColor = color1
        self.LightPercent.textColor = color2
        self.ElectricPercent.textColor = color3
        self.HVAClabel.textColor = color1
        self.lightLabel.textColor = color2
        self.electricLabel.textColor = color3
        self.pieChartView.delegate = self
        self.pieChartView.noDataText = "Loading Data..."
        self.pieChartView.noDataFont = UIFont(name:"Futura (Light)", size: UIScreen.main.bounds.width/15)
        self.pieChartView.noDataTextColor = appBlack
        self.pieChartView.backgroundColor = UIColor.clear
        self.pieChartView.holeColor = UIColor.clear
        self.pieChartView.holeRadiusPercent = 0.9
        self.pieChartView.legend.enabled = false
        self.pieChartView.chartDescription?.enabled = false
        self.pieChartView.highlightPerTapEnabled = false
        self.makePieChart()
        let myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15), NSAttributedStringKey.foregroundColor: appBlack]
        self.pieChartView.centerAttributedText = NSAttributedString(string: "\(userStatus.energyValue) W"
            , attributes: myAttributes)
    }

    @objc func energyLineChartSetup() {
        self.lineChartView.delegate = self
        self.lineChartView.noDataText = "Loading Data..."
        self.lineChartView.noDataFont = UIFont(name:"Futura (Light)", size: UIScreen.main.bounds.width/15)
        self.lineChartView.noDataTextColor = appBlack
        self.lineChartView.chartDescription?.text = "Tap to view"
        self.lineChartView.chartDescription?.textColor = appBlack
        self.lineChartView.gridBackgroundColor = UIColor.clear
        self.lineChartView.xAxis.labelTextColor = appBlack
        self.lineChartView.leftAxis.labelTextColor = appBlack
        self.lineChartView.rightAxis.labelTextColor = appBlack
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        
        self.lineChartView.backgroundColor = UIColor.clear
        self.lineChartView.doubleTapToZoomEnabled = false
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.rightAxis.drawGridLinesEnabled = false
        //self.lineChartView.leftAxis.drawGridLinesEnabled = false
        self.lineChartView.rightAxis.drawAxisLineEnabled = false
        self.lineChartView.leftAxis.drawAxisLineEnabled = false
        self.lineChartView.drawBordersEnabled = false
        self.lineChartView.drawMarkers = false
        self.lineChartView.xAxis.drawAxisLineEnabled = false
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.rightAxis.drawLabelsEnabled = false
        self.lineChartView.extraRightOffset = 10.0
        self.lineChartView.dragEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.leftAxis.axisMinimum = 0.0
        self.lineChartView.leftAxis.valueFormatter = myValueFormatter()
        self.lineChartView.xAxis.valueFormatter = timeValueFormatter()
        self.lineChartView.xAxis.setLabelCount(13, force: true)
    }
/////////////////Popover///////////////

    @IBAction func popover(_ sender: Any) {
        self.performSegue(withIdentifier: "showView", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showView")
        {
            let vc = segue.destination
            let controller = vc.popoverPresentationController
            if (controller != nil){
                controller?.delegate = self
                self.view.alpha = 0.4;
            }
        }
    }
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        self.view.alpha = 1.0;
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

//////////////////////////////////////
    
    
    //Called every time the pie chart is updated, every 4 seconds
    @objc func makePieChart() {
        var dataEntries: [ChartDataEntry] = [ChartDataEntry]()
        if (userStatus.energyValue == 0) {
            self.HVACpercent.text = "0 W"
            self.LightPercent.text = "0 W"
            self.ElectricPercent.text = "0 W"
        } else {
            let HVACper = userStatus.HVAC
            self.HVACpercent.text = "\(HVACper) W"
            let Lightper = userStatus.light
            self.LightPercent.text = "\(Lightper) W"
            let Electricper = userStatus.electric
            self.ElectricPercent.text = "\(Electricper) W"
        }
        let myAttributes = [NSAttributedStringKey.font: UIFont(name:"Futura (Light)", size:UIScreen.main.bounds.width/15), NSAttributedStringKey.foregroundColor: appBlack]
        self.pieChartView.centerAttributedText = NSAttributedString(string: "\(userStatus.energyValue) W"
            , attributes: myAttributes)
        if (userStatus.energyValue != 0) {
        dataEntries.append(ChartDataEntry(x: 0, y: userStatus.HVAC))
        dataEntries.append(ChartDataEntry(x: 1, y: userStatus.light))
        dataEntries.append(ChartDataEntry(x: 2, y: userStatus.electric))
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "PieChartEnergy")
        pieChartDataSet.drawValuesEnabled = false
        var dataSets: [PieChartDataSet] = [PieChartDataSet]()
        dataSets.append(pieChartDataSet)
        let pieChartData = PieChartData(dataSets: dataSets)
        self.pieChartView.data = pieChartData
        let colors: [UIColor] = [color1, color2, color3]
        pieChartDataSet.colors = colors
        } else {
            dataEntries.append(ChartDataEntry(x: 0, y: 1))
            let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "PieChartEnergy")
            pieChartDataSet.drawValuesEnabled = false
            var dataSets: [PieChartDataSet] = [PieChartDataSet]()
            dataSets.append(pieChartDataSet)
            let pieChartData = PieChartData(dataSets: dataSets)
            self.pieChartView.data = pieChartData
            let colors: [UIColor] = [appGrey]
            pieChartDataSet.colors = colors
        }
    }
    
    //REALLY JANKY WAY TO ENSURE WE ONLY SET UP ONCE
    //if we only do setChartData() the first time, otherwise we do updateCounter()
    @objc func makeChart() {
        if userStatus.vals.count == 0 {
            return
        }
        if (self.reset == false) {
            self.reset = true
            setChartData()
        } else {
            updateCounter()
        }
    }
    
    //Called the FIRST TIME we add a new (HVAC, light, electric) data point, one for each line
    @objc func setChartData() {
        var HVACVals: [ChartDataEntry] = [ChartDataEntry]()
        var lightVals: [ChartDataEntry] = [ChartDataEntry]()
        var electricVals: [ChartDataEntry] = [ChartDataEntry]()
        for i in 0...(userStatus.HVACvals.count-1) {
            electricVals.append(ChartDataEntry(x: Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i] + userStatus.electricVals[i])))
            lightVals.append(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i])))
            HVACVals.append(ChartDataEntry(x: Double(i), y: Double(userStatus.HVACvals[i])))
        }
        let set1: LineChartDataSet = LineChartDataSet(values: HVACVals, label: "HVAC Set")
        let set2: LineChartDataSet = LineChartDataSet(values: lightVals, label: "light Set")
        let set3: LineChartDataSet = LineChartDataSet(values: electricVals, label: "electric Set")
        
        
        set1.fill = Fill(color: color1)
        set1.drawFilledEnabled = true
        set2.fill = Fill(color: color2)
        set2.drawFilledEnabled = true
        set3.fill = Fill(color: color3)
        set3.drawFilledEnabled = true
        set1.drawValuesEnabled = false
        set2.drawValuesEnabled = false
        set3.drawValuesEnabled = false
        
        
        set1.setColor(color1)
        set1.setCircleColor(appGrey)
        set1.lineWidth = 2.0
        set1.circleRadius = 3.0
        set1.fillAlpha = 150/255.0
        set1.highlightColor = appGrey
        set1.drawCircleHoleEnabled = true
        set2.setColor(color2)
        set2.setCircleColor(appGrey)
        set2.lineWidth = 2.0
        set2.circleRadius = 3.0
        set2.fillAlpha = 150/255.0
        set2.highlightColor = appGrey
        set2.drawCircleHoleEnabled = true
        set3.setColor(color3)
        set3.setCircleColor(appGrey)
        set3.lineWidth = 2.0
        set3.circleRadius = 3.0
        set3.fillAlpha = 150/255.0
        set3.highlightColor = appGrey
        set3.drawCircleHoleEnabled = true
        var dataSets: [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(set1)
        dataSets.append(set2)
        dataSets.append(set3)
        let data:LineChartData = LineChartData(dataSets: dataSets)
        data.setValueTextColor(appGrey)
        self.lineChartView.data = data
        self.lineChartView.data?.notifyDataChanged()
        let start = max(0, userStatus.vals.count-13)
        self.lineChartView.moveViewToX(Double(start))
        self.lineChartView.setVisibleXRange(minXRange: Double(12.0), maxXRange: Double(12.0))
    }
    
    
    //Called every time to add a new (HVAC, light, electric) data point
    @objc func updateCounter() {
        let i = userStatus.HVACvals.count-1
        self.lineChartView.data?.addEntry(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i])), dataSetIndex: 2)
        self.lineChartView.data?.addEntry(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i])), dataSetIndex: 1)
        self.lineChartView.data?.addEntry(ChartDataEntry(x:Double(i), y: Double(userStatus.HVACvals[i] + userStatus.lightVals[i] + userStatus.electricVals[i])), dataSetIndex: 0)
        let _ = self.lineChartView.data?.removeEntry(xValue: Double(i-13), dataSetIndex: 0)
        let _ = self.lineChartView.data?.removeEntry(xValue: Double(i-13), dataSetIndex: 1)
        let _ = self.lineChartView.data?.removeEntry(xValue: Double(i-13), dataSetIndex: 2)
        self.lineChartView.data?.notifyDataChanged()
        self.lineChartView.notifyDataSetChanged()
        let start = i
        self.lineChartView.moveViewToAnimated(xValue: Double(start), yValue: Double(userStatus.HVACvals[i]), axis: YAxis.AxisDependency.left, duration: 0.8, easingOption: ChartEasingOption.easeInSine)
        self.lineChartView.setVisibleXRange(minXRange: Double(12.0), maxXRange: Double(12.0))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

