//
//  AboutUsViewController.swift
//  EnergyApp
//
//  Created by Peter Wei on 3/27/17.
//  Copyright © 2017 Rong Zhou. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController, UITextViewDelegate {

    @IBOutlet var wholeView: UIView!
    @IBOutlet var CommentsSection: UIView!
    
    @IBOutlet var myTextView: UITextView!

    @IBAction func sendComment(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        self.myTextView.delegate = self
        // Do any additional setup after loading the view.
    }
    

    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
    override func viewDidAppear(_ animated: Bool) {
    }

    @objc func keyboardWillShow() {
        bottomConstraint.constant = 216
        UIView.animate(withDuration: 0.3, animations: {self.view.layoutIfNeeded()})
    }
    @objc func keyboardWillHide() {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: {self.view.layoutIfNeeded()})
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
